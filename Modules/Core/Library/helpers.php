<?php


use Nwidart\Modules\Facades\Module;

if (!function_exists("Address_Project")) {

    function Address_Project(string $url)
    {

        return Module::asset($url);

    }
}


if(! function_exists('Address_Module')){

    function Address_Module(string $name){

        return Module::assetPath($name) ;
    };

}

if(! function_exists('hasModule')){

    function hasModule(string $module){

        return array_key_exists($module,Module::getByStatus(1)) ?true :false ;
    };

}

