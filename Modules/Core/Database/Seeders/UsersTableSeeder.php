<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $users=[

            [
                'name' => 'Amin Nourbaghaei',
                'email' => 'noorbaghaei.a2017@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('secret'),
                'mobile' => '09195995044',
                'code' => rand(1000,999999),
                'username' => 'amin',
                'avatar' => null,
                'status' => 1,
                'two_step' => null,
                'identity_card' => '_Amin_'
            ]
        ];

        DB::table('users')->insert($users);
    }
}
