<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


//Route::group(["namespace" => "Auth"], function () {
//
//    $this->get('login', 'LoginController@showLoginForm')->name('login');
//
//
//    // Registration Routes...
//    $this->post('login', 'LoginController@login');
//    $this->post('logout', 'LoginController@logout')->name('logout');
//
//
//    // Password Reset Routes...
//    $this->get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
//    $this->post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
//    $this->get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
//    $this->post('password/reset', 'ResetPasswordController@reset');
//
//
//});

Auth::routes();

Route::group([ "middleware" => ["auth:web"]], function () {

    Route::get('/dashboard', 'CoreController@index')->name('dashboard.website');

});



Route::group([ "middleware" => ["auth:web"]], function()
{
    Route::get('/', 'CoreController@website')->name('front.website');
});
