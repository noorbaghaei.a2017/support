<!DOCTYPE html>
<html lang="fa-IR">
<head>
    <meta charset="utf-8"/>
    <title>@yield('pageTitle') - {{config('app.name')}}</title>
    <meta name="description" content="@yield('description')"/>
    <meta name="keywords" content="@yield('keywords')"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="{{asset('assets/images/logo.png')}}">
    <meta name="apple-mobile-web-app-title" content="uinvest">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="{{asset('assets/images/logo.png')}}">
    <link rel="stylesheet" href="{{asset('assets/css/animate.css/animate.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/glyphicons/glyphicons.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome/css/font-awesome.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/material-design-icons/material-design-icons.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/ionicons/css/ionicons.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/simple-line-icons/css/simple-line-icons.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap/dist/css/bootstrap.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/styles/app.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/styles/style.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/styles/font.css')}}" type="text/css"/>
    <link href="{{asset('assets/css/bootstrap-rtl/dist/bootstrap-rtl.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/styles/app.rtl.css')}}" rel="stylesheet"/>
</head>
<body>
<div class="app" id="app">
    <div id="aside" class="app-aside fade nav-dropdown black">
        <div class="navside dk" data-layout="column">
            <div class="navbar no-radius">
                <a href="#" class="navbar-brand">
                    <div data-ui-include="'images/logo.svg'"></div>
                    <img src="{{asset('assets/images/logo.png')}}" alt="{{config('app.name')}}" class="hide">
                    <span class="hidden-folded inline">{{config('app.name')}}</span>
                </a>
            </div>
            {{--@if(auth()->user()->role == 3)--}}
                {{--<div data-flex class="hide-scroll">--}}
                    {{--<nav class="scroll nav-stacked nav-stacked-rounded nav-color">--}}
                        {{--<ul class="nav" data-ui-nav>--}}
                            {{--<li class="nav-header hidden-folded">--}}
                                {{--<span class="text-xs">داوود</span>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{route('index')}}" class="b-danger">--}}
                                  {{--<span class="nav-icon text-white no-fade">--}}
                                    {{--<i class="ion-filing"></i>--}}
                                  {{--</span>--}}
                                    {{--<span class="nav-text">داشبورد</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{route('dcompanies.index')}}" class="b-info">--}}
                              {{--<span class="nav-icon text-white no-fade">--}}
                                {{--<i class="ion-arrow-graph-up-right"></i>--}}
                              {{--</span>--}}
                                    {{--<span class="nav-text">تحلیلگران</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{route('dpayments.index')}}" class="b-info">--}}
                                  {{--<span class="nav-icon text-white no-fade">--}}
                                    {{--<i class="fa fa-dollar"></i>--}}
                                  {{--</span>--}}
                                    {{--<span class="nav-text">پرداختی‌ها</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{route('dusers.index')}}" class="b-info">--}}
                              {{--<span class="nav-icon text-white no-fade">--}}
                                {{--<i class="ion-person"></i>--}}
                              {{--</span>--}}
                                    {{--<span class="nav-text">کاربران</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{route('ddevices.index')}}" class="b-info">--}}
                                  {{--<span class="nav-icon text-white no-fade">--}}
                                    {{--<i class="ion-ios-monitor"></i>--}}
                                  {{--</span>--}}
                                    {{--<span class="nav-text">دستگاه ها</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{route('dsignals.index')}}" class="b-info">--}}
                                  {{--<span class="nav-icon text-white no-fade">--}}
                                    {{--<i class="fa fa-signal"></i>--}}
                                  {{--</span>--}}
                                    {{--<span class="nav-text">سیگنال ها</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{route('dtrades.index')}}" class="b-info">--}}
                                  {{--<span class="nav-icon text-white no-fade">--}}
                                    {{--<i class="ion-briefcase"></i>--}}
                                  {{--</span>--}}
                                    {{--<span class="nav-text"> معاملات</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{route('dservers.index')}}" class="b-info">--}}
                                  {{--<span class="nav-icon text-white no-fade">--}}
                                    {{--<i class="ion-android-cloud"></i>--}}
                                  {{--</span>--}}
                                    {{--<span class="nav-text">سرور ها</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{route('dsubscriptions.index')}}" class="b-info">--}}
                                  {{--<span class="nav-icon text-white no-fade">--}}
                                    {{--<i class="ion-help-buoy"></i>--}}
                                  {{--</span>--}}
                                    {{--<span class="nav-text">اشتراک ها</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</nav>--}}
                {{--</div>--}}
            {{--@endif--}}
            <div data-flex class="hide-scroll">
                <nav class="scroll nav-stacked nav-stacked-rounded nav-color">
                    <ul class="nav" data-ui-nav>
                        <li class="nav-header hidden-folded">
                            <span class="text-xs">دسترسی سریع</span>
                        </li>
                        <li>
                            <a href="#" class="b-danger">
                                  <span class="nav-icon text-white no-fade">
                                    <i class="ion-filing"></i>
                                  </span>
                                <span class="nav-text">پیشخوان</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="ion-arrow-graph-up-right"></i>
                              </span>
                                <span class="nav-text">منو</span>
                            </a>
                        </li>


                    </ul>
                </nav>
            </div>

        </div>
    </div>
    <div id="content" class="app-content box-shadow-z2 bg pjax-container" role="main">
        <div class="app-header white bg b-b">
            <div class="navbar" data-pjax>
                <a data-toggle="modal" data-target="#aside" class="navbar-item pull-left hidden-lg-up p-r m-a-0">
                    <i class="ion-navicon"></i>
                </a>
                <div class="navbar-item pull-left h5" id="pageTitle">@yield('pageTitle')</div>
                <ul class="nav navbar-nav pull-right">
                    <li class="nav-item"><a href="{{route('front.website')}}" target="_blank" class="btn btn-sm text-sm btn-primary text-white m-t-1">سایت اصلی</a></li>
                    <li class="nav-item dropdown">
                        <a class="nav-link clear" data-toggle="dropdown">
                            <span class="avatar w-32">
                                <img src="{{asset('assets/images/a3.jpg')}}" class="w-full rounded" alt="#">
                            </span>
                        </a>
                        <div class="dropdown-menu w dropdown-menu-scale pull-right">
                            <a class="dropdown-item" href="#">
                                <span>پروفایل</span>
                            </a>
                            <a class="dropdown-item"
                               onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">خروج</a>
                            <form id="frm-logout" action="#" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="app-footer white bg p-a b-t">
            <div class="pull-right text-sm text-muted">نسخه {{config('hodhod.version')}}</div>
            <span class="text-sm text-muted">&copy;تمامی حقوق برای <strong>«هیربد»</strong> محفوظ است. </span>
        </div>
        <div class="app-body">
        <div class="padding">
            <div class="row">
                @yield('content')
            </div>
        </div>
        </div>
    </div>
</div>
<script src="{{asset('assets/libs/jquery/dist/jquery.js')}}"></script>
<script src="{{asset('assets/libs/tether/dist/js/tether.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap/dist/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/libs/jQuery-Storage-API/jquery.storageapi.min.js')}}"></script>
<script src="{{asset('assets/libs/PACE/pace.min.js')}}"></script>
<script src="{{asset('assets/libs/jquery-pjax/jquery.pjax.js')}}"></script>
<script src="{{asset('assets/libs/blockUI/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/libs/jscroll/jquery.jscroll.min.js')}}"></script>
<script src="{{asset('assets/scripts/config.lazyload.js')}}"></script>
<script src="{{asset('assets/scripts/ui-load.js')}}"></script>
<script src="{{asset('assets/scripts/ui-jp.js')}}"></script>
<script src="{{asset('assets/scripts/ui-include.js')}}"></script>
<script src="{{asset('assets/scripts/ui-device.js')}}"></script>
<script src="{{asset('assets/scripts/ui-form.js')}}"></script>
<script src="{{asset('assets/scripts/ui-modal.js')}}"></script>
<script src="{{asset('assets/scripts/ui-nav.js')}}"></script>
<script src="{{asset('assets/scripts/ui-list.js')}}"></script>
<script src="{{asset('assets/scripts/ui-screenfull.js')}}"></script>
<script src="{{asset('assets/scripts/ui-scroll-to.js')}}"></script>
<script src="{{asset('assets/scripts/ui-toggle-class.js')}}"></script>
<script src="{{asset('assets/scripts/ui-taburl.js')}}"></script>
<script src="{{asset('assets/scripts/app.js')}}"></script>
<script src="{{asset('assets/scripts/ajax.js')}}"></script>
@yield('scripts')
{{--<script type="text/javascript">!function(){function t(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,localStorage.getItem("rayToken")?t.src="https://app.raychat.io/scripts/js/"+o+"?rid="+localStorage.getItem("rayToken")+"&href="+window.location.href:t.src="https://app.raychat.io/scripts/js/"+o;var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=document,a=window,o="5f826723-5803-4a03-82df-a6b5f52eb326";"complete"==e.readyState?t():a.attachEvent?a.attachEvent("onload",t):a.addEventListener("load",t,!1)}();</script>--}}
</body>
</html>
