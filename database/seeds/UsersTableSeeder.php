<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->delete();


        $user=[
           [
               'name'=>"amin",
               'mobile'=>"09195995044",
               'token'=>Str::random(),
               "email"=>"noorbaghaei.a2017@gmail.com",
               "password"=>Hash::make('secret'),
               "created_at"=>now()
           ]

        ];


        DB::table('users')->insert($user);

    }
}
