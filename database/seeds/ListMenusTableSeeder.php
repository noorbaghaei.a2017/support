<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ListMenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('list_menus')->delete();


        $user=[
            [
                'label'=>"منو بالا",
                'name'=>"top-menu",
                'token'=>Str::random()
            ]

        ];


        DB::table('list_menus')->insert($user);

    }
}
