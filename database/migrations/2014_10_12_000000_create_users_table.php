<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username', 45)->unique()->nullable();
            $table->string('token')->unique();
            $table->string('avatar')->nullable();
            $table->string('mobile', 11)->unique()->nullable();
            $table->string('code', 6)->unique()->nullable();
            $table->string('name')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('two_step')->nullable();
            $table->string('identity_card')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
