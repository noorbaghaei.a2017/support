<?php

namespace App\View;

use Illuminate\View\View;
use Modules\Plan\Entities\Plan;

class frontComposer{


    public function compose(View $view){

        $view->with('plans', Plan::latest()->get());

    }

}
