<?php

namespace App\View;

use App\User;
use Illuminate\View\View;
use Modules\Article\Entities\Article;
use Modules\Information\Entities\Information;
use Modules\Plan\Entities\Plan;
use Modules\Product\Entities\Product;

class adminComposer{

    public function compose(View $view){

        $view->with('articlescount', Article::latest()->count());
        $view->with('productscount', Product::latest()->count());
        $view->with('informationscount', Information::latest()->count());
        $view->with('userscount', User::latest()->count());

    }

}
